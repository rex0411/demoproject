//
//  ContainerController.swift
//  collectionDemo
//
//  Created by rexC on 30/06/20.
//  Copyright © 2020 rexC. All rights reserved.
//

import UIKit

class ContainerController: UIViewController{
    
//    MARK: - Properties
    
    var menuController : MenuController!
    var centerController: UIViewController!
    var isExpanded = false
    
//    MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureHomeControler()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation{
        return .slide
    }
    
    override var prefersStatusBarHidden: Bool{
        return isExpanded
    }
//    MARK: - Handlers
    
    func configureHomeControler() {
        let homeController = HomeController()
        homeController.delegate = self
        centerController = UINavigationController(rootViewController: homeController)
        
        view.addSubview(centerController.view)
        addChild(centerController)
        centerController.didMove(toParent: self)
    }
    
    func configureMenuController() {
        if menuController == nil{
//            add menuController Here
            menuController = MenuController()
            menuController.delegate = self
            view.insertSubview(menuController.view, at: 0)
            addChild(menuController)
            menuController.didMove(toParent: self)
            
        }
        
    }
    func animatePanel(shouldExpand:Bool, menuOption: MenuOption?){
        if shouldExpand {
//            show mennu
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 80
            }, completion: nil)
        }else{
//            hide menu
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = 0
            }){(_) in
                guard let menuOption = menuOption else { return }
                self.didSelectMenuOption(menuOption: menuOption)
            }
        }
        animatedStatusBar()
    }
    func didSelectMenuOption(menuOption: MenuOption){
        switch menuOption{
            
        case .Profile:
            print("Show profile")
        case .Inbox:
            print("Show Index")
        case .Notification:
            print("Notification")
        case .Settings:
            let controller = SettingController()
            present(UINavigationController(rootViewController: controller), animated: true, completion: nil )
        }
    }
    func animatedStatusBar(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: nil)
    }
}

extension ContainerController: HomeControllerDelegate{
    func handleMenuToggle(forMenuOption menuOption: MenuOption?) {
        if !isExpanded {
             configureMenuController()
         }
         isExpanded = !isExpanded
        
        animatePanel(shouldExpand: isExpanded, menuOption: menuOption)
        
    }
    
}
