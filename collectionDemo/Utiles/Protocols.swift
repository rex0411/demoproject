//
//  Protocols.swift
//  collectionDemo
//
//  Created by rexC on 30/06/20.
//  Copyright © 2020 rexC. All rights reserved.
//

protocol HomeControllerDelegate {
    func handleMenuToggle(forMenuOption manuOption: MenuOption?)
}
