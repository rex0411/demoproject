//
//  SettingsController.swift
//  collectionDemo
//
//  Created by rexC on 06/07/20.
//  Copyright © 2020 rexC. All rights reserved.
//

import UIKit

class SettingController: UIViewController{
    
//    MARK - Properties
    
//    MARK - Init
    override func viewDidLoad() {
        self.viewDidLoad()
        configureUI()
    }
//    MARK -Selectors
    @objc func handleDismiss(){
        dismiss(animated: true, completion: nil)
    }
//    MARK - Helper function
    func configureUI(){
        view.backgroundColor = .purple
        navigationController?.navigationBar.barTintColor = .darkGray
        navigationItem.title = "Settings"
        navigationController?.navigationBar.barStyle = .black
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "multiply-white-50").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleDismiss))
    }
    
//    MARK - Handlers
    
}
