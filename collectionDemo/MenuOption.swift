//
//  MenuOptions.swift
//  collectionDemo
//
//  Created by rexC on 02/07/20.
//  Copyright © 2020 rexC. All rights reserved.
//

import UIKit

enum MenuOption: Int, CustomStringConvertible {
    
    case Profile
    case Inbox
    case Notification
    case Settings
    
    var description: String {
        switch self {
        case .Profile: return "Profile"
        case .Inbox: return "Inbox"
        case .Notification: return "Notification"
        case .Settings: return "Settings"
            
        }
    }
    var image: UIImage {
        
        switch self {
        case .Profile: return UIImage(named: "user-white-50") ?? UIImage()
        case .Inbox: return UIImage(named: "envelope-white-50") ?? UIImage()
        case .Notification: return UIImage(named: "notification-white-50") ?? UIImage()
        case .Settings: return UIImage(named: "settings-white-50") ?? UIImage()
        
        }
        
    }
}
